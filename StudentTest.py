
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import unittest, time, re
import CommonData
from StudentListPage import StudentListPage
from StudentDetailPage import StudentDetailPage as stD


class StudentTestCases(unittest.TestCase):

    def setUp(self):
        self.driver = CommonData.common_driver
        self.base_url = CommonData.base_url
        self.driver.implicitly_wait(30)

    def stest_studentlistpage(self):
        """ SMOKE TEST STUDENT LIST """
        driver = self.driver
        searchContent = "sun"
        driver.get(self.base_url + "/ciie.html#/student/studentList")
        
        sl = StudentListPage(driver)
        # input student Name
        sl.inputStudentName(searchContent)
        WebDriverWait(driver, 20).until(EC.invisibility_of_element_located((By.CLASS_NAME, "fullScreenLoading")))
        # click my  button
        sl.clickMyButton()
        try:
            sl.clickSearchButton()
            driver.find_elements_by_id("DataTables_Table_1")
        except NoSuchElementException:
            print ("Not find search result!")
        
        resultelement=driver.find_element_by_xpath("//*[@id='page-wrapper']/div[2]/div[2]/div/div[2]/div[1]/div/div[2]/div[1]/div[2]/h3").text
        
        match = re.search(r'(\w+)\s+(\w+)\s+', resultelement)
        if match:
            self.assertEqual(match.group(2).lower(),searchContent)
        else:
            print("search not match!")
            
        CommonData.common_driver = driver
        CommonData.base_url = self.base_url
        
    def test_studentdetailedpage(self):
        """ SMOKE TEST STUDENT DETAILED PAGE """
        driver = self.driver
        st = stD(driver)
        # click student list page show more button
        driver.get(self.base_url + "/ciie.html#/student/studentList")
        driver.find_element_by_xpath("//*[@id='page-wrapper']/div[2]/div[2]/div/div[2]/div[1]/div/div[1]/div/a").click()
        # click tab and check exception
        try:
            # click summary tab
            st.clickTab("summary")
            st.clickTab("student information")
            st.clickTab("people")
            st.clickTab("host family")
            st.clickTab("documents")
            st.clickTab("payments")
            st.clickTab("student updates")
            st.clickTab("notes")
            st.clickTab("school")
            st.clickTab("travel")
            st.clickTab("application")
            
            
        except NoSuchElementException:
            print ("Click Student Detailed Tab have problem ")
        # assert every tab name 
        tablist = ["summary","student information", "people", "host family", "documents", "payments", "student updates", "notes", "school"]
        for i in range(len(tablist)):
            
            tabname=st.getTabText(tablist[i])
            print(" ASSERT " + tablist[i]+ " --")
            
                
                
            tabname_low = tabname.lower()
            self.assertEqual(tablist[i], tabname_low)
        
        CommonData.common_driver = driver
        CommonData.base_url = self.base_url

    def tearDown(self):
        pass



if __name__ == "__main__":
    unittest.main()
