from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

from BasePage import BasePage

class StudentDetailPage(BasePage):

    summarytab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[1]/a")
    studentinfotab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[2]/a")
    peopletab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[3]/a")
    hostfamilytab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[4]/a")
    documenttab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[5]/a")
    paymenttab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[6]/a")
    studentupdatetab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[7]/a")
    notestab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[8]/a")
    schooltab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[9]/a")
    traveltab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[10]/a")
    applicationtab = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[2]/div/ul/li[11]/a")
    updatebutton = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[2]/div/div[2]/button[1]")
    startappliationbutton = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[2]/div/div[2]/button[2]")
    updateresidentailbutton = ('XPATH',"//*[@id='page-wrapper']/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[2]/div/div[2]/button[3]")

    
    def __init__(self,browser):
        super(StudentDetailPage,self).__init__(browser)

    def getTabText(self, tabname = ''):
        tabElement = None
        if tabname == 'summary':
            tabElement = self.findElement(self.summarytab)
        elif tabname == 'student information':
            tabElement = self.findElement(self.studentinfotab)
        elif tabname == 'people':
            tabElement = self.findElement(self.peopletab)
        elif tabname == 'host family':
            tabElement = self.findElement(self.hostfamilytab)
        elif tabname == 'documents':
            tabElement = self.findElement(self.documenttab)
        elif tabname == 'payments':
            tabElement = self.findElement(self.paymenttab)
        elif tabname == 'student updates':
            tabElement = self.findElement(self.studentupdatetab)
        elif tabname == 'notes':
            tabElement = self.findElement(self.notestab)
        elif tabname == 'school':
            tabElement = self.findElement(self.schooltab)    
        elif tabname == 'travel':
            tabElement = self.findElement(self.traveltab)
        elif tabname == 'application':
            tabElement = self.findElement(self.applicationtab)
        else:
            print("Tab name not found, Please check your tab name!")
        txt = self.getText(tabElement)
        return txt
        
    def clickTab(self,tabname):
        if tabname == 'summary':
            tabElement = self.findElement(self.summarytab)
        elif tabname == 'student information':
            tabElement = self.findElement(self.studentinfotab)
        elif tabname == 'people':
            tabElement = self.findElement(self.peopletab)
        elif tabname == 'host family':
            tabElement = self.findElement(self.hostfamilytab)
        elif tabname == 'documents':
            tabElement = self.findElement(self.documenttab)
        elif tabname == 'payments':
            tabElement = self.findElement(self.paymenttab)
        elif tabname == 'student updates':
            tabElement = self.findElement(self.studentupdatetab)
        elif tabname == 'notes':
            tabElement = self.findElement(self.notestab)
        elif tabname == 'school':
            tabElement = self.findElement(self.schooltab)    
        elif tabname == 'travel':
            tabElement = self.findElement(self.traveltab)
        elif tabname == 'application':
            tabElement = self.findElement(self.applicationtab)    
        else:
            self.click(tabElement)
        

    def clickUpdateSButton():
        button = self.findElement(self.updatebutton)
        self.click(button)
        
    def clickStarAButton():
        button = self.findElement(self.startappliationbutton)
        self.click(button)
        
    def clickUpdateRButton():
        button = self.findElement(self.updateresidentailbutton)
        self.click(button)


if __name__ == '__main__':
    driver = webdriver.Firefox()
    driver.get("https://staging.ciiedu.net/ciie.html#/public/login")
    driver.implicitly_wait(5)
    driver.find_element_by_xpath("//input[@type='text']").send_keys("jingjing.fan")
    driver.find_element_by_xpath("//input[@type='password']").clear()
    #driver.find_element_by_xpath("//input[@type='password']").send_keys("test123")
    driver.find_element_by_xpath("//input[@type='password']").send_keys("Harmony2016")
    driver.find_element_by_xpath("//button[@type='submit']").click()
    time.sleep(5)
    driver.get("https://staging.ciiedu.net/ciie.html#/student/studentList")
    time.sleep(5)
   
    driver.find_element_by_xpath("//*[@id='page-wrapper']/div[2]/div[2]/div/div[2]/div[1]/div/div[1]/div/a").click()
    s = StudentDetailPage(driver)
    #s.clickTab('student information')
    s.getTabText('student information')
    
