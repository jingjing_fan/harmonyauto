
# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time
import unittest
import CommonData

class LoginPage(unittest.TestCase):
        
    def setUp(self):
        self.base_url = "https://staging.ciiedu.net/"
    
        
    #def GoTo(self):
        #self.driver.get(self.base_url + "/ciie.html#/public/login")
        

    def test_LoginAs(self):
        """ TEST USER LOGIN HARMONY """
        
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(5)
        self.driver.get(self.base_url + "/ciie.html#/public/login")        
        
        self.driver.find_element_by_xpath("//input[@type='text']").clear()
        #self.driver.find_element_by_xpath("//input[@type='text']").send_keys("autoscript")
        self.driver.find_element_by_xpath("//input[@type='text']").send_keys("jingjing.fan")
        self.driver.find_element_by_xpath("//input[@type='password']").clear()
        #self.driver.find_element_by_xpath("//input[@type='password']").send_keys("test123")
        self.driver.find_element_by_xpath("//input[@type='password']").send_keys("Harmony2016")
        self.driver.find_element_by_xpath("//button[@type='submit']").click()
        CommonData.common_driver = self.driver
        CommonData.base_url = self.base_url

    def GoTo(self,sub_url):
        self.driver.get( self.base_url + sub_url)

   
    def tearDown(self):
        pass
 
if __name__ == "__main__":
    unittest.main()

    
