from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

from BasePage import BasePage


class StudentListPage(BasePage):
    studentnamebox = ('XPATH',"//*[@id='page-wrapper']/div[2]/div[2]/div/div[1]/div[1]/div[2]/form/div/div[1]/div/input")
    studentidbox = ('XPATH',"//*[@id='page-wrapper']/div[2]/div[2]/div/div[1]/div/div[2]/form/div[1]/div[1]/div/input")
    studentemailbox = ('XPATH',"//*[@id='page-wrapper']/div[2]/div[2]/div/div[1]/div[1]/div[2]/form/div/div[3]/div/input")
    countryselection = ('XPATH',"//*[@id='page-wrapper']/div[2]/div[2]/div/div[1]/div[1]/div[2]/form/div/div[4]/div/div/a/span" )
    agencyselection = ('XPATH',"//*[@id='page-wrapper']/div[2]/div[2]/div/div[1]/div[1]/div[2]/form/div/div[6]/div/div/ins")
    Mybox = ('XPATH',"//*[@id='page-wrapper']/div[2]/div[2]/div/div[1]/div[1]/div[2]/form/div/div[6]/div/div/ins")
    searchbutton = ('XPATH',"//*[@id='page-wrapper']/div[2]/div[2]/div/div[1]/div[1]/div[2]/form/div/div[7]/button[2]")
    
    def __init__(self,browser):
        super(StudentListPage,self).__init__(browser)
        

    def inputStudentName(self, searchContent):
        
        searchBox = self.findElement(self.studentnamebox)
        self.type(searchBox,searchContent)
        self.enter(searchBox)
        
    def inputStudentID(self,searchContent):
        searchBox = self.findElement(self.studentidbox)
        self.type(searchBox,searchContent)
        self.enter(searchBox)

    def clickMyButton(self):
        inputBox = self.findElement(self.Mybox)
        self.click(inputBox)
        
    def clickSearchButton(self):
        searchBox = self.findElement(self.searchbutton)
        self.click(searchBox)
        



if __name__ == '__main__':
    driver = webdriver.Firefox()
    driver.get("https://uat.ciiedu.net/ciie.html#/public/login")
    driver.implicitly_wait(5)
    driver.find_element_by_xpath("//input[@type='text']").send_keys("jingjing.fan")
    driver.find_element_by_xpath("//input[@type='password']").clear()
    #driver.find_element_by_xpath("//input[@type='password']").send_keys("test123")
    driver.find_element_by_xpath("//input[@type='password']").send_keys("Harmony2016")
    driver.find_element_by_xpath("//button[@type='submit']").click()
    driver.get("https://uat.ciiedu.net/ciie.html#/student/studentList")
    st = StudentListPage(driver)
    
    st.inputStudentName('as')
    st.clickMyButton()
                         

